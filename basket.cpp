#include <cassert>
#include <cmath>
#include <time.h>
#include<cstdlib>

using namespace std;


// DEFINE AQUI LA CLASE BBPlayer

class BBPlayer
{
private:
    string _name;
    int _shotsTaken;
    int _shotsMade;
    int _rebounds;
    int _score;
    int _gamesPlayed;

public:

    BBPlayer();
    string name();
    void setAll(string a, int b, int c, int d, int e);
    void shotMade();
    void shotMissed();
    void reboundMade();
    void addGame();
    void printStats() const;
    float shotPercentage() const;
    float reboundsPerGame() const;
    int score();


};

string BBPlayer::name()
{
    return _name;
}

void BBPlayer::setAll(string a, int b, int c, int d, int e)
{

    _name = a;
    _shotsTaken = b;
    _shotsMade = c;
    _rebounds = e;
    _gamesPlayed = d;

}

void BBPlayer::shotMade()
{

    _shotsMade = _shotsMade + 1;
    _shotsTaken = _shotsTaken + 1;
    _score = _score + 2;

}

void BBPlayer::shotMissed()
{

    _shotsTaken = _shotsTaken + 1;

}

void BBPlayer::reboundMade()
{

   _rebounds = _rebounds + 1;

}

void BBPlayer::addGame()
{

    _gamesPlayed = _gamesPlayed + 1;

}


int BBPlayer::score()
{

    return _score;

}



BBPlayer::BBPlayer() {
    _shotsTaken = _shotsMade = _gamesPlayed = _rebounds = _score = 0;
}




float BBPlayer::shotPercentage() const {
    return _shotsTaken == 0 ? .5 : static_cast<float>(_shotsMade) / _shotsTaken;
}




float BBPlayer::reboundsPerGame() const {
    return _gamesPlayed == 0 ? 3 : static_cast<float>(_rebounds) / _gamesPlayed;
}





void BBPlayer::printStats() const {
    cout << "Stats for "     << _name << endl;
    cout << "Games played: " << _gamesPlayed << endl;
    cout << "Rebounds: "     << _rebounds << endl;
    cout << "Shots taken: "  << _shotsTaken << endl;
    cout << "Shots made: "   << _shotsMade << endl;

}



void test_BBPlayer() {

    BBPlayer T;
  T.setAll("Chemba", 100, 65, 10, 35);

    // Test the name() method
  assert( T.name() == "Chemba" );

    // Test the initial shot pct
 assert( abs( T.shotPercentage() - 0.65) < 0.00001 );

    // Test after a shot is made
  T.shotMade();
  assert( abs( T.shotPercentage() - (1.0 * 66 / 101) ) < 0.00001) ;

    // Test after a shot is missed
   T.shotMissed();
   assert( abs( T.shotPercentage() - (1.0 * 66 / 102) ) < 0.00001 );

    // Test the initial rebounds per game
  assert( abs( T.reboundsPerGame() - 3.5 ) < 0.00001 );

    // Test the score up to this point
  assert( T.score()  == 2);

    // Make two shots, then test score and pct
  T.shotMade();
  T.shotMade();
  assert( T.score()  == 6);
  assert( abs( T.shotPercentage() - (1.0 * 68 / 104) ) < 0.00001 );

    // Make two rebounds then test
  T.reboundMade();
  T.reboundMade();
  assert( abs( T.reboundsPerGame() - 3.7 ) < 0.00001 );

    // Tests if the number of game has increased.
  T.addGame();
  assert( abs( T.reboundsPerGame() - (1.0 * 37 / 11) ) < 0.00001 );

    cout << "All unit tests passed!!!" << endl;
}

int main()
{
    test_BBPlayer();

  const int GOAL_SCORE = 32;

//  create the two players
 BBPlayer P[2];
 P[0].setAll("Chemba", 100, 65, 10, 35);
 P[1].setAll("Wes", 100, 56, 10, 1);


    srand(time(NULL));

    // the index of the player in offense, randomly determined
    int playerIdx = rand() % 2;

    // the player who won, -1 if no one has won
    int playerWon = -1;

    // the result of a dice throw, a number between [0.0,1.0]
    float diceThrow;

    cout << "The game starts...." << endl;
    P[0].addGame();
    P[1].addGame();

    while (playerWon == -1) {

        // the offensive player takes a shot
      cout << P[playerIdx].name() << " shoots .... ";
      diceThrow = static_cast<float>( rand() ) / RAND_MAX;

        // determine if shot is made based on shooting percentage

      if ( diceThrow < P[playerIdx].shotPercentage() ) {

          cout << "and scores!!!" << endl;
          P[playerIdx].shotMade();
          cout << "The score is " << P[0].name() << ":"<< P[0].score()
               << " to " << P[1].name() << ":"<< P[1].score() << endl;

          if (P[playerIdx].score() >= GOAL_SCORE) playerWon = playerIdx;
          else playerIdx = (playerIdx + 1) % 2;
      }
      else {
          cout << "and misses :-(" << endl;
          P[playerIdx].shotMissed();

            // determine who gets the rebound

          float chanceOfense  = P[playerIdx].reboundsPerGame();
          float chanceDefense = P[(playerIdx + 1) % 2].reboundsPerGame() * 2;

          diceThrow = static_cast<float>( rand() ) / RAND_MAX;
          diceThrow = diceThrow * ( chanceOfense + chanceDefense);

          playerIdx = diceThrow < chanceOfense ? playerIdx : (playerIdx + 1) % 2;
          cout << P[playerIdx].name() << " gets the rebound .... " << endl;

      }
  }

  cout << P[playerWon].name() << " wins the game!" << endl << endl;
  cout << "Final statistics . . . " << endl;
  cout << "---------------------------" << endl;
  P[0].printStats();
  cout << "---------------------------" << endl;
  P[1].printStats();
  cout << "---------------------------" << endl;

    return 0;
}
